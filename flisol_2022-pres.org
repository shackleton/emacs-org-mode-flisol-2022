#+REVEAL_ROOT: file://///home/shackleton/Proyectos/itechile.cl/debianday/presentation/reveal.js/
#+OPTIONS: num:nil toc:nil
#+REVEAL_TRANS: slide
#+REVEAL_THEME: black
#+TITLE: GNU Emacs and org-mode
#+AUTHOR: Sebastian Godoy Olivares
#+SUBTITLE: Iniciacion en emacs org-mode
#+Email: shackleton@riseup.net

* Introducción
#+REVEAL: split
Un editor de texto extensible, personalizable, libre, gratuito y muchas cosas mas.
#+NAME:   fig:Logo.

#+REVEAL: split
“Estoy usando Linux. Una biblioteca que emacs usa para comunicarse con el hardware de Intel”.[fn:a1]
[fn:a1]– Erwin, emacs, Freenode.


* El camino de Emacs
#+REVEAL: split
Resulta que Emacs es una filosofía o incluso una religión. Asi que,
la broma sobre la "Iglesia de Emacs" es inquietantemente precisa en
de muchas maneras
#+REVEAL: split
La primera y (más obvia) audiencia son personas nuevas en
Emacs. Si nunca ha usado Emacs antes en su vida, usted
Espero que este libro le resulte muy útil. Sin embargo, si
eres nuevo en Emacs y no eres técnico, entonces vas
para pasarlo mas dificil. Emacs, a pesar de ser adecuado para
mucho más que programar, está directamente dirigido a
gente con conocimientos de informática. Aunque es perfectamente posible
usar Emacs de todos modos, este libro asumirá que usted está
técnicamente inclinado, pero no necesariamente un programador.

#+REVEAL: split


#+REVEAL: split
Los koanes de Ruby, es un desafió para no programadores, mediante codificación en Ruby.

#+REVEAL: split
Si quieres iniciarte en la programación llegaste a un buen documento, que puede servirte de auto-iniciación o ayuda en la mejora de la técnica. No necesitas ningún conocimiento previo en programación, pero si un deseo profundo de aprender a programar.

#+REVEAL: split
Un día solo estas copiando y pegando código, sin entender en absoluto lo que estas haciendo, y a otro todo tiene un sentido claro y preciso. La siguiente metodología no es algo nuevo, pero si inusual en la formación clásica de programadores.

Puedo resumir esta técnica de aprendizaje como:
"Una mente vacía, es mas útil que una mente con dudas"

* Que es Ruby?

- Ruby es un lenguaje multiplataforma, interpretado y orientado a objetos, dinámico y de código abierto enfocado en la simplicidad y productividad.

- Ruby fue diseñado por Yukihiro Matsumoto ('Matz') en 1993, con el Principio de la Menor Sorpresa: Matz dice "quería minimizar mi frustración mientras programo, y eso conllevaba minimizar mi esfuerzo. Este es el principal objetivo de Ruby. *Quiero divertirme mientras programo*. Después de lanzar Ruby y probarlo mucha gente, ellos me dijeron que sentían lo mismo que yo. Ellos fueron los que acuñaron el término de "Principio de Menor Sorpresa".
- En el año 2004 hubo un boom en el interés por Ruby, debido a Ruby on Rails: el entorno para desarrollo web de David Heinemeier Hansson.

 Su elegante sintaxis se siente natural al leerla y fácil al escribirla.

#+begin_src ruby
# Ruby sabe lo que tu
# quieres, aun si quieres
# hacer matemáticas
# en un Arreglo completo
ciudades = %w[ Londres
              Oslo
              París
              Ámsterdam
              Berlín ]
visitado = %w[Berlín Oslo]

puts "Aun necesito " +
     "visitar las " +
     "siguientes ciudades:",
     ciudades - visitado
#+end_src

#+RESULTS:
: nil

* Que es un kōan?


- Un kōan (公案; japonés: kōan, del chino: gōng'àn) es, en la tradición zen, un problema que el maestro plantea al alumno para comprobar sus progresos. Muchas veces el kōan parece un problema absurdo (véase: aporía), ilógico o banal. Para resolverlo el novicio debe desligarse del pensamiento racional común para así entrar en un sentido racional más elevado y así aumentar su nivel de conciencia para intuir lo que en realidad le está preguntando el maestro, que trasciende al sentido literal de las palabras.

- Los koans son acertijos que el maestro de zen entrega a sus discípulos para ayudarles a desarrollar el pensamiento lateral, es decir, la parte más intuitiva de su mente.

- Cuando la mente esta quieta, cuando la mente está en silencio, adviene lo nuevo. En esos instantes, la ESENCIA, el BUDDHATA, se escapa del intelecto y en ausencia del YO experimenta ESO que no es del tiempo...


* Que son los Ruby Koans?

Los Koans te acompañan por el camino hacia la iluminación para aprender Ruby. El objetivo es aprender el lenguaje, la sintaxis, la estructura de Ruby y algunas funciones y bibliotecas comunes. También la cultura. Probar no es solo algo a lo que prestamos atención, sino algo en lo que vivimos. Es esencial en su búsqueda aprender y hacer grandes cosas en el idioma.


Los koans se dividen en áreas por archivo, los hash se tratan en about_hashes.rb, los módulos se introducen en about_modules.rb, etc. Se presentan en orden en el archivo path_to_enlightenment.rb.

Cada koan aumenta su conocimiento de Ruby y se basa en sí mismo. Se detendrá en el primer lugar que necesite corregir.

Algunos koans simplemente necesitan que se sustituya la respuesta correcta por una incorrecta. Algunos, sin embargo, requieren que proporciones tu propia respuesta. Si ve el método __ (un subrayado doble) en la lista, es una sugerencia para que proporcione su propio código para que funcione correctamente.

* Instalando Ruby en ambientes Debían GNU/Linux

* Descargando el proyecto Ruby koan
https://github.com/edgecase/ruby_koans/blob/master/download/rubykoans.zip


