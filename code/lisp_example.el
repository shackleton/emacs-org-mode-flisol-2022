;;; lisp_example.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Ernest Henry Shackleton
;;
;; Author: Ernest Henry Shackleton <shackleton@riseup.net>
;; Maintainer: Ernest Henry Shackleton <shackleton@riseup.net>
;; Created: abril 21, 2022
;; Modified: abril 21, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/shackleton/lisp_example
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:



(provide 'lisp_example)
;;; lisp_example.el ends here
#! /usr/bin/gcl -f

; (getArrSlize arr rowNum)
; ------------------------
; given an array and a row number,
;    get one row of an array as a vector
(defun getArrSlice (arr rowNum)
   (make-array (array-dimension arr 1)
      :displaced-to arr
         :displaced-index-offset (* rowNum (array-dimension arr 1))))

; try it out
(defvar arr (make-array '(3 4) :initial-element 1))
(format t "~%as array:~%~A~%~%" arr)
(format t "~%as list:~%~A~%~%" (getArrSlice arr 1))
